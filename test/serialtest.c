#include <errno.h>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

	int
set_interface_attribs (int fd, int speed, int parity)
{
	struct termios tty;
	memset (&tty, 0, sizeof tty);
	if (tcgetattr (fd, &tty) != 0)
	{
		//error_message ("error %d from tcgetattr", errno);
		perror("tcgetattr");
		return -1;
	}

	cfsetospeed (&tty, speed);
	cfsetispeed (&tty, speed);
	cfmakeraw(&tty);

	tty.c_cc[VMIN]  = 1;            // read doesn't block
	tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

	tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
	tty.c_cflag &= ~CSTOPB;			// 1 stop bit
	tty.c_cflag &= ~CRTSCTS;

	if (tcsetattr (fd, TCSANOW, &tty) != 0)
	{
		//error_message ("error %d from tcsetattr", errno);
		perror("tcsetattr");
		return -1;
	}
	return 0;
}

	void
set_blocking (int fd, int should_block)
{
	struct termios tty;
	memset (&tty, 0, sizeof tty);
	if (tcgetattr (fd, &tty) != 0)
	{
		//error_message ("error %d from tggetattr", errno);
		perror("tggetattr");
		return;
	}

	tty.c_cc[VMIN]  = should_block ? 1 : 0;
	tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

	if (tcsetattr (fd, TCSANOW, &tty) != 0)
		//error_message ("error %d setting term attributes", errno);
		perror("tcsetattr");
}

int main(int argc, char **argv)
{
	const char portname[] = "/dev/ttyUSB0";
	int fd = open(portname, O_RDWR | O_NOCTTY);
	if(fd < 0)
	{
		perror(portname);
		return 1;
	}		
	set_interface_attribs (fd, B9600, 0);  // set speed to 115,200 bps, 8n1 (no parity)
	char buffer[16] = {0};

	while(1)
	{
		write(fd, "msg", 3);
		usleep(50000);
	}	

	while(1)
	{
		int n = read (fd, buffer, 16);
		printf("read %d bytes: %s\n", n, buffer);
		// echo back the thing we read
	}

}











