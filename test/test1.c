#define PXA_IMPLEMENTATION
#include "../include/api.h"

int main(int argc, char** argv)
{
	pxa_init();
	PXA_MSG msg;
    msg.type = 1;
    msg.vec3f = (VEC3F){1,2,3};

    pxa_send_async(msg, PXA_PRIORITY_HIGH);
	return 0;
}
