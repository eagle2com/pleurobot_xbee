//@file: api.h
//@author: Peter Lichard
//@brief: header-only library for the communication between a controller and a robot

/* To use this API, please #define PXA_IMPLEMENTATION in ONE place right 
 * BEFORE including this file(probably in the main file) 
 * All functions you should use start with pxa_, the ones starting with
 * _pxa_ are internal.
 *
 * You just need to link with -lpthread in order to use transmit functions asynchronously
 *
 * You can #define PXA_TX and/or PXA_RX to override the default port settings (0,1)
 * The same thing goes for:
 * - PXA_LOCAL_ID and PXA_REMOTE_ID
 * - PXA_BAUD_RATE
 * - PXA_MAX_QUEUE
 * */

#ifndef PXA_API_H
#define PXA_API_H

#define _POSIX_C_SOURCE	199309L

// Prevent gcc from complaining about nanosleep


#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <time.h>


// DEFINITION

// Defines
#ifndef PXA_TX
#define PXA_TX 0
#endif
//TODO: Change these numbers to some sensible gpio ports on the ODroid
#ifndef PXA_RX
#define PXA_RX 1
#endif

#ifndef PXA_LOCAL_ID
#define PXA_LOCAL_ID 0
#endif

#ifndef PXA_REMOTE_ID
#define PXA_REMOTE_ID 1
#endif

#ifndef PXA_BAUD_RATE
#define PXA_BAUD_RATE 9600
#endif 

#ifndef PXA_MAX_QUEUE
#define PXA_MAX_QUEUE 100
#endif

#define byte unsigned char
#define PXA_HIGH 1
#define PXA_LOW 0

typedef enum PXA_PRIORITY {PXA_PRIORITY_LOW, PXA_PRIORITY_HIGH} PXA_PRIORITY;

typedef struct
{
    float x,y,z;
} VEC3F;

typedef struct
{
    int x,y,z;
} VEC3I;

typedef struct
{
    byte type;
    union
	{
        VEC3I vec3i;
        VEC3F vec3f;
    };
    byte checksum;
} PXA_MSG;

// Interface Functions
int pxa_init();
int pxa_close();

int pxa_send(PXA_MSG msg);
int pxa_send_async(PXA_MSG msg, PXA_PRIORITY priority);
int pxa_recv(PXA_MSG *out);
byte pxa_checksum(const PXA_MSG *msg);

// Internal Functions - DO NOT USE THESE UNLESS YOU KNOW WHAT YOU ARE DOING
int _pxa_write_byte(byte value);
int _pxa_write_bytes(const byte *buffer, size_t length);
int _pxa_read_byte(byte *out);
int _pxa_read_bytes(byte *buffer, size_t length);

void _pxa_sort_queue();

// IMPLEMENTATION
#ifdef PXA_IMPLEMENTATION

#define GPIO_IMPLEMENTATION
#include "gpio.h"

static PXA_MSG _pxa_msg_lqueue[PXA_MAX_QUEUE];
static PXA_MSG _pxa_msg_hqueue[PXA_MAX_QUEUE];
static int _pxa_msg_lqueue_size = 0;
static int _pxa_msg_hqueue_size = 0;

int pxa_init()
{
    _pxa_msg_lqueue_size = 0;
    _pxa_msg_hqueue_size = 0;

    //TODO: Enable this for the release
    //gpio_open(PXA_TX, GPIO_OUT);
    //gpio_open(PXA_RX, GPIO_IN);

    return 0;
}

int pxa_close()
{
    //TODO: Enable this for the release
    //gpio_close(PXA_TX);
    //gpio_close(PXA_RX);
}

int pxa_send(PXA_MSG msg)
{
    msg.checksum = pxa_checksum(&msg);
    int n = _pxa_write_bytes((byte*)&msg, sizeof(PXA_MSG));
    return n;
}

int pxa_recv(PXA_MSG *out)
{
    int n = _pxa_read_bytes((byte*)out, sizeof(PXA_MSG));
    byte sum = pxa_checksum(out);
    if(out->checksum != sum)
    {
        return 1;
    }
    return 0;
}

int pxa_send_async(PXA_MSG msg, PXA_PRIORITY priority)
{
    if(priority == PXA_PRIORITY_HIGH)
    {
        if(_pxa_msg_hqueue_size < PXA_MAX_QUEUE)
        {
            _pxa_msg_hqueue[_pxa_msg_lqueue_size++] = msg;
        }
    }
    else
    {
    }
    return 0;
}

byte pxa_checksum(const PXA_MSG *msg)
{
    byte sum = 0;
    for(int i = 0; i < sizeof(PXA_MSG) - 1; i++)
    {
        sum += ((byte*)msg)[i];
    }
    return sum;
}

int _pxa_write_byte(byte value)
{
	int port = 0; //placeholder
	// Write start bit (LOW)
	port = PXA_LOW;
	
	// And now 8 bits, LSB first

    struct timespec ts = {};
    ts.tv_nsec = 1000000000/PXA_BAUD_RATE;

    port = (value >> 0) & 0x1; nanosleep(&ts, NULL);
    port = (value >> 1) & 0x1; nanosleep(&ts, NULL);
    port = (value >> 2) & 0x1; nanosleep(&ts, NULL);
    port = (value >> 3) & 0x1; nanosleep(&ts, NULL);
    port = (value >> 4) & 0x1; nanosleep(&ts, NULL);
    port = (value >> 5) & 0x1; nanosleep(&ts, NULL);
    port = (value >> 6) & 0x1; nanosleep(&ts, NULL);
    port = (value >> 7) & 0x1; nanosleep(&ts, NULL);

	// Always set the port to high when not transmitting
    port = PXA_HIGH; nanosleep(&ts, NULL);
	return 0;
}

int _pxa_write_bytes(const byte *buffer, size_t length)
{
	size_t n = 0;
	while(n < length)
	{
		_pxa_write_byte(buffer[n]);	
		n++;
	}
	return n;
}

int _pxa_read_byte(byte *out)
{
    struct timespec ts = {};
    ts.tv_nsec = 1000000000/(2*PXA_BAUD_RATE);

	int port = 0; //placeholder
	//TODO: Can we interrupt?
	while(port == PXA_HIGH)
        nanosleep(&ts, NULL);

    ts.tv_nsec = 1000000000/PXA_BAUD_RATE;

	*out = 0;
    *out |= (port&0x1) << 0; nanosleep(&ts, NULL);
    *out |= (port&0x1) << 1; nanosleep(&ts, NULL);
    *out |= (port&0x1) << 2; nanosleep(&ts, NULL);
    *out |= (port&0x1) << 3; nanosleep(&ts, NULL);
    *out |= (port&0x1) << 4; nanosleep(&ts, NULL);
    *out |= (port&0x1) << 5; nanosleep(&ts, NULL);
    *out |= (port&0x1) << 6; nanosleep(&ts, NULL);
    *out |= (port&0x1) << 7; nanosleep(&ts, NULL);
	return 0;
}

int _pxa_read_bytes(byte *buffer, size_t length)
{
	size_t n = 0;
	while(n < length)
	{
		_pxa_read_byte(&buffer[n]);
		n++;
	}
	return n;
}


#endif // PXA_API_H




#endif // PXA_IMPLEMENTATION
