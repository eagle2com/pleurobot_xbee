#ifndef GPIO_H
#define GPIO_H

//@file: gpio.h
//@author: Peter Lichard
//@brief: header-only library for managing gpios on an ODroid

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>

#ifndef GPIO_MAX
#define GPIO_MAX 32
#endif

#ifndef GPIO_SYSFS_DIR
#define GPIO_SYSFS_DIR  "/sys/class/aml_gpio/"
#endif

typedef enum GPIO_DIR {GPIO_IN, GPIO_OUT} GPIO_DIR;
typedef enum GPIO_VALUE {GPIO_HIGH, GPIO_LOW} GPIO_VALUE;

typedef struct
{
  GPIO_DIR dir;
  GPIO_VALUE value;
  unsigned char is_open;
  unsigned char fd_value;           // File descriptor
  unsigned char fd_direction;
} _GPIO_INFO;

// DEFINITIONS

int gpio_open(int n, GPIO_DIR dir);
int gpio_close(int n);
int gpio_close_all(void);
int gpio_poll(int n);
int gpio_write(int n, GPIO_VALUE value);
int gpio_read(int n);

#ifdef GPIO_IMPLEMENTATION
static _GPIO_INFO _gpio_ports[GPIO_MAX] = {};

int gpio_poll(int n)
{
    assert(_gpio_ports[n].is_open);
    read(_gpio_ports[n].fd_value, &_gpio_ports[n].value, 1);
    return 0;
}

int gpio_write(int n, GPIO_VALUE value)
{
    assert(_gpio_ports[n].is_open && _gpio_ports[n].dir == GPIO_OUT);

    if(value == GPIO_HIGH)
    {
        write(_gpio_ports[n].fd_value, "1", 2);
    }
    else
    {
        write(_gpio_ports[n].fd_value, "0", 2);
    }
}

int gpio_read(int n)
{
    return _gpio_ports[n].value;
}

int gpio_close(int n)
{
    if(_gpio_ports[n].is_open)
    {
        close(_gpio_ports[n].fd_value);
        close(_gpio_ports[n].fd_direction);
    }
    return 0;
}

int gpio_close_all()
{
    for(int i = 0; i < GPIO_MAX; i++)
    {
        gpio_close(i);
    }
    return 0;
}

int gpio_open(int n, GPIO_DIR dir)
{
  static char buffer[64] = {};
  if(!_gpio_ports[n].is_open)
  {
    snprintf(buffer, 64, GPIO_SYSFS_DIR "/gpio%d/direction", n);
    _gpio_ports[n].fd_direction = open(buffer, O_WRONLY);
    if(_gpio_ports[n].fd_direction < 0)
    {
      perror(buffer);
      return 1;
    }

    snprintf(buffer, 64, GPIO_SYSFS_DIR "/gpio%d/value", n);
    if(dir == GPIO_IN)
    {
      _gpio_ports[n].fd_value = open(buffer, O_RDONLY);
      write(_gpio_ports[n].fd_direction, "in", 3);
    }
    else if(dir == GPIO_OUT)
    {
      _gpio_ports[n].fd_value = open(buffer, O_WRONLY);
      write(_gpio_ports[n].fd_direction, "out", 4);
    }
    if(_gpio_ports[n].fd_value < 0)
    {
      perror(buffer);
      return 1;
    }

    _gpio_ports[n].is_open = 1;
  }
}

#endif // GPIO_IMPLEMENTATION

#endif // GPIO_H
